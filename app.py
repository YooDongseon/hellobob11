#BoB 11기 교육생 유동선(8792)
from os import lseek
from flask import Flask, request
app = Flask(__name__)

class Test_Class:
    def add(self, a, b):
        return a+b
    def sub(self, a, b):
        return a-b

class App_Class:
       @app.route('/')
       def index():
              return 'Hello Bob from user106'

       @app.route('/add')
       def add():    # a 와 b 두개의 인자를 GET 파라미터를 통해 받아 덧셈 결과를 출력
              a = request.args.get('a', 1, type=int)
              b = request.args.get('b', 1, type=int)
              result = a+b
              return str(result)

       @app.route('/sub')
       def sub():    # a 와 b 두개의 인자를 GET 파라미터를 통해 받아 뺄셈 결과를 출력
              a = request.args.get('a', 1, type=int)
              b = request.args.get('b', 1, type=int)
              result = a-b
              return str(result)

if __name__ == "__main__":
       c = Test_Class()
       app.run(host='0.0.0.0', port=8106)