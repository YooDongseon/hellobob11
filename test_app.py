# BoB 11기 교육생 유동선(8792)
# test_add() 및 test_sub() 작성
import unittest
import app

class Test(unittest.TestCase):
    def test_add(self):
        a = app.Test_Class()
        res = a.add(1, 2)
        self.assertEqual(res, 3)

    def test_sub(self):
        s = app.Test_Class()
        res = s.sub(1, 2)
        self.assertEqual(res, -1)

if __name__ == "__main__":
    unittest.main()